﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Svg;

namespace ImageProcessor
{
    public class ImageProcessor
    {
        public enum FigureType
        {
            isosceles_triangle,
            square,
            scalene_triangle,
            parallelogram,
            equilateral_triangle,
            pentagon,
            rectangle,
            hexagon,
            heptagon,
            octagon,
            circle,
            oval,
            ellipse,
            none
        }
        public enum MeasurementType
        {
            radius,
            diameter,
            length,
            width,
            height,
            angle,
            none
        }
        
        public Color FillColor = Color.Green;
        public Color StrokeColor = Color.Black;
        public int StrokeWidth = 1;
        public static int maxMeasurement = 1000;

        private static FigureType[] _enumCache = null;
        public static FigureType GetFigureType(string query)
        {
            if (_enumCache == null)
                _enumCache = Enum.GetNames(typeof(FigureType)).Select(p => (FigureType)Enum.Parse(typeof(FigureType), p)).ToArray();
            foreach (var e in _enumCache)
            {
                if (query.Contains(e.ToString().Replace("_", " ")))
                    return e;
            }
            return FigureType.none;
        }

        private static MeasurementType[] _measCache = null;
        public static List<KeyValuePair<MeasurementType, int>> GetMeasurementTypes(string query)
        {
            if (_measCache == null)
                _measCache = Enum.GetNames(typeof(MeasurementType)).Select(p => (MeasurementType)Enum.Parse(typeof(MeasurementType), p)).ToArray();
            var res = new List<KeyValuePair<MeasurementType, int>>();
            foreach (var e in _measCache)
            {
                while (true)
                {
                    var m = Regex.Match(query, e.ToString() + @"\s*[ofis]{0,4}\s*([\d]+)");
                    if (!m.Success) break;
                    var value = int.Parse(m.Groups[1].Value);
                    if (value > maxMeasurement)
                        value = maxMeasurement;
                    res.Add(new KeyValuePair<MeasurementType, int>(e, value));
                    query = query.Substring(0, query.IndexOf(m.Value)) + query.Substring(query.IndexOf(m.Value) + m.Value.Length);
                }
            }
            return res;
        }

        /// <summary>
        /// Get value of measurement
        /// </summary>
        /// <param name="meas">List of measurements</param>
        /// <param name="type">Type of measurement</param>
        /// <param name="strong">If not strong, than method try give value from other measurements. For example, length from width.</param>
        /// <returns></returns>
        private static int getMeasurement(List<KeyValuePair<MeasurementType, int>> meas, MeasurementType type, bool strong = true)
        {
            var r = 0;
            MeasurementType t = MeasurementType.none;
            switch (type)
            {
                case MeasurementType.radius:
                    r = getMeasurementNative(meas, MeasurementType.radius, out t);
                    if (strong) break;
                    if (r == 0)
                        r = getMeasurementNative(meas, MeasurementType.diameter, out t) / 2;
                    if (r == 0)
                        r = getMeasurement(meas, MeasurementType.width, false) / 2;
                    break;
                case MeasurementType.diameter:
                    r = getMeasurementNative(meas, MeasurementType.diameter, out t);
                    if (strong) break;
                    if (r == 0)
                        r = getMeasurementNative(meas, MeasurementType.radius, out t) * 2;
                    break;
                case MeasurementType.width:
                    r = getMeasurementNative(meas, MeasurementType.width, out t);
                    if (strong) break;
                    if (r == 0)
                        r = getMeasurementNative(meas, MeasurementType.length, out t);
                    break;
                case MeasurementType.height:
                    r = getMeasurementNative(meas, MeasurementType.height, out t);
                    if (strong) break;
                    if (r == 0)
                        r = getMeasurementNative(meas, MeasurementType.length, out t);
                    break;
                case MeasurementType.length:
                    r = getMeasurementNative(meas, MeasurementType.length, out t);
                    if (strong) break;
                    if (r == 0)
                        r = getMeasurementNative(meas, MeasurementType.width, out t);
                    break;
                case MeasurementType.angle:
                    r = getMeasurementNative(meas, MeasurementType.angle, out t);
                    break;
            }

            meas.Remove(meas.FirstOrDefault(p => p.Key == t && p.Value == r));
            return r;
        }

        /// <summary>
        /// Just get value of measurement
        /// </summary>
        /// <returns></returns>
        private static int getMeasurementNative(List<KeyValuePair<MeasurementType, int>> meas, MeasurementType type, out MeasurementType t)
        {
            t = type;
            foreach (var kv in meas)
            {
                if (kv.Key == type)
                    return kv.Value;
            }
            return 0;
        }

        /// <summary>
        /// Main method. Get image from query
        /// </summary>
        public byte[] GetImage(string query)
        {
            query = query.ToLower();
            var type = GetFigureType(query); // get type od query
            if (type != FigureType.none)
            {
                var meas = GetMeasurementTypes(query); // get measurements
                if (meas.Count > 0)
                {
                    SvgElement elem = null;
                    KeyValuePair<MeasurementType, int> kv;
                    int i1, i2, a;
                    switch (type)
                    {
                        case FigureType.circle:
                            i1 = getMeasurement(meas, MeasurementType.radius, false);
                            if (i1 != 0)
                            {
                                elem = GetCircle(i1);
                            }
                            break;
                        case FigureType.square:
                            i1 = getMeasurement(meas, MeasurementType.length, false);
                            if (i1 != 0)
                            {
                                elem = GetSquare(i1);
                            }
                            break;
                        case FigureType.rectangle:
                            i2 = getMeasurement(meas, MeasurementType.height, true);
                            i1 = getMeasurement(meas, MeasurementType.width, false);
                            if (i1 != 0)
                            {
                                elem = GetRectangle(i1, i2);
                            }
                            break;
                        case FigureType.isosceles_triangle:
                            i1 = getMeasurement(meas, MeasurementType.length, false);
                            i2 = getMeasurement(meas, MeasurementType.height, false);
                            if (i1 != 0 && i2 != 0)
                            {
                                elem = GetIsoscelesTriangle(i1, i2);
                            }
                            break;
                        case FigureType.scalene_triangle:
                            i1 = getMeasurement(meas, MeasurementType.length, false);
                            i2 = getMeasurement(meas, MeasurementType.length, false);
                            a = getMeasurement(meas, MeasurementType.angle, false);
                            if (i1 != 0 && i2 != 0)
                            {
                                elem = GetScaleneTriangle(i1, i2, a);
                            }
                            break;
                        case FigureType.equilateral_triangle:
                            i1 = getMeasurement(meas, MeasurementType.length, false);
                            if (i1 != 0)
                            {
                                elem = GetEquilateralTriangle(i1);
                            }
                            break;
                        case FigureType.parallelogram:
                            i1 = getMeasurement(meas, MeasurementType.length, false);
                            i2 = getMeasurement(meas, MeasurementType.length, false);
                            a = getMeasurement(meas, MeasurementType.angle, false);
                            if (i1 != 0 && i2 != 0)
                            {
                                elem = GetParallelogram(i1, i2, a);
                            }
                            break;
                        case FigureType.pentagon:
                            i1 = getMeasurement(meas, MeasurementType.length, false);
                            if (i1 != 0)
                            {
                                elem = GetXgon(i1, 5);
                            }
                            break;
                        case FigureType.hexagon:
                            i1 = getMeasurement(meas, MeasurementType.length, false);
                            if (i1 != 0)
                            {
                                elem = GetXgon(i1, 6);
                            }
                            break;
                        case FigureType.heptagon:
                            i1 = getMeasurement(meas, MeasurementType.length, false);
                            if (i1 != 0)
                            {
                                elem = GetXgon(i1, 7);
                            }
                            break;
                        case FigureType.octagon:
                            i1 = getMeasurement(meas, MeasurementType.length, false);
                            if (i1 != 0)
                            {
                                elem = GetXgon(i1, 8);
                            }
                            break;
                        case FigureType.oval:
                        case FigureType.ellipse:
                            i2 = getMeasurement(meas, MeasurementType.height, true);
                            i1 = getMeasurement(meas, MeasurementType.width, false);
                            if (i1 != 0)
                            {
                                elem = GetEllipse(i1, i2);
                            }
                            break;

                    }
                    if (elem != null && elem is SvgVisualElement)
                    {
                        elem.Fill = new SvgColourServer(FillColor);
                        elem.Stroke = new SvgColourServer(StrokeColor);
                        elem.StrokeWidth = StrokeWidth;
                        var bounds = ((SvgVisualElement)elem).Bounds;
                        if (elem is SvgPolygon)
                            bounds = getBounds(elem as SvgPolygon);
                        SvgDocument doc = new Svg.SvgDocument() { Width = bounds.Width + 20, Height = bounds.Height + 20 };
                        doc.ViewBox = new SvgViewBox(bounds.X - 10, bounds.Y - 10, bounds.Width + 20, bounds.Height + 20);
                        doc.Children.Add(elem);
                        var stream = new MemoryStream();
                        doc.Write(stream);
                        var res = stream.GetBuffer().Take((int)stream.Length).ToArray();
                        stream.Dispose();
                        return res;
                    }
                }
            }
            return new byte[0];
        }

        public SvgElement GetCircle(int r)
        {
            return new SvgCircle()
            {
                Radius = r
            };
        }

        public SvgElement GetEllipse(int l1, int l2)
        {

            return new SvgEllipse()
            {
                CenterX = new SvgUnit(0),
                CenterY = new SvgUnit(0),
                RadiusX = new SvgUnit(l1),
                RadiusY = new SvgUnit(l2)
            };
        }

        public SvgElement GetSquare(int l)
        {
            return new SvgRectangle()
            {
                Width = l,
                Height = l
            };
        }

        public SvgElement GetRectangle(int w, int h)
        {
            return new SvgRectangle()
            {
                Width = w,
                Height = h
            };
        }

        public SvgElement GetIsoscelesTriangle(int l, int h)
        {
            List<Point> points = new List<Point>();
            points.Add(new Point(-l / 2, h / 2));
            points.Add(new Point(l / 2, h / 2));
            points.Add(new Point(0, -h / 2));
            var res = new SvgPolygon
            {
                Points = new SvgPointCollection()
            };
            res.Points.AddRange(points.Select(p => new[] { p.X, p.Y }).SelectMany(p => p).Select(p => new SvgUnit(p)));
            return res;
        }

        public SvgElement GetScaleneTriangle(int l1, int l2, int a)
        {
            List<Point> points = new List<Point>();
            a = -a;
            points.Add(new Point(0, 0));
            points.Add(new Point(l1, 0));
            points.Add(fromPolar(l2, a));
            var res = new SvgPolygon
            {
                Points = new SvgPointCollection()
            };
            res.Points.AddRange(points.Select(p => new[] { p.X, p.Y }).SelectMany(p => p).Select(p => new SvgUnit(p)));
            return res;
        }

        public SvgElement GetEquilateralTriangle(int l)
        {
            List<Point> points = new List<Point>();
            points.Add(new Point(-l / 2, 0));
            points.Add(new Point(l / 2, 0));
            points.Add(new Point(0, -(int)(l * Math.Sqrt(3) / 2)));
            var res = new SvgPolygon
            {
                Points = new SvgPointCollection()
            };
            res.Points.AddRange(points.Select(p => new[] { p.X, p.Y }).SelectMany(p => p).Select(p => new SvgUnit(p)));
            return res;
        }

        public SvgElement GetParallelogram(int l1, int l2, int a)
        {
            List<Point> points = new List<Point>();
            a = -a;
            var _p = fromPolar(l2, a);
            points.Add(new Point(0, 0));
            points.Add(new Point(l1, 0));
            points.Add(new Point(_p.X + l1, _p.Y));
            points.Add(_p);
            var res = new SvgPolygon
            {
                Points = new SvgPointCollection()
            };
            res.Points.AddRange(points.Select(p => new[] { p.X, p.Y }).SelectMany(p => p).Select(p => new SvgUnit(p)));
            return res;
        }

        public SvgElement GetXgon(int l, int x)
        {
            List<Point> points = new List<Point>();
            var da = 360 / x;
            var R = (int)(l / (2 * Math.Sin(toRad(180 / x))));
            for (var a = da / 2; a < 360; a += da)
            {
                points.Add(fromPolar(R, a));
            }

            var res = new SvgPolygon
            {
                Points = new SvgPointCollection()
            };
            res.Points.AddRange(points.Select(p => new[] { p.X, p.Y }).SelectMany(p => p).Select(p => new SvgUnit(p)));
            return res;
        }

        private static Point fromPolar(int l, int a)
        {
            return new Point((int)(l * Math.Cos(toRad(a))), (int)(l * Math.Sin(toRad(a))));
        }

        private static double toRad(double g)
        {
            return g * Math.PI / 180;
        }

        private static double toGrad(double r)
        {
            return r * 180 / Math.PI;
        }

        private static RectangleF getBounds(SvgPolygon elem)
        {
            float maxX = -10000, minX = 10000, maxY = -10000, minY = 10000;
            for (int i = 0; i < elem.Points.Count; i++)
            {
                var v = elem.Points[i].Value;
                if (i % 2 == 0)
                {
                    if (v > maxX) maxX = v;
                    if (v < minX) minX = v;
                }
                else
                {
                    if (v > maxY) maxY = v;
                    if (v < minY) minY = v;
                }
            }
            var w = maxX - minX;
            var h = maxY - minY;
            return new RectangleF(minX, minY, w, h);
        }

        public static Color FromHtmlString(string str, Color @default)
        {
            if (string.IsNullOrEmpty(str)) return @default;
            try
            {
                str = str.Trim(' ', '#');
                var r = int.Parse(str.Substring(0, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                var g = int.Parse(str.Substring(2, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                var b = int.Parse(str.Substring(4, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                return Color.FromArgb(r, g, b);
            }
            catch (Exception ex)
            {
                return @default;
            }
        }
    }
}