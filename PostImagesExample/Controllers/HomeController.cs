﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PostImagesExample.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Title = "TestImage";
            return View();
        }

        /// <summary>
        /// This metod gives an image for query in data
        /// </summary>
        /// <returns></returns>
        public ActionResult Image()
        {
            try
            {
                var data = Request["data"];
                var color = Request["color"];
                var strokecolor = Request["strokecolor"];
                var strokewidth = Request["strokewidth"];
                int strokewidthInt = 0;
                if (string.IsNullOrEmpty(strokewidth) || !int.TryParse(strokewidth, out strokewidthInt))
                    strokewidthInt = 0;
                //if (Request.IsAjaxRequest())
                var proc = new ImageProcessor.ImageProcessor()
                {
                    FillColor = ImageProcessor.ImageProcessor.FromHtmlString(color, System.Drawing.Color.Green),
                    StrokeColor = ImageProcessor.ImageProcessor.FromHtmlString(strokecolor, System.Drawing.Color.Black),
                    StrokeWidth = strokewidthInt
                };
                var imageBytes = proc.GetImage(data);
                return base.File(imageBytes, "image/svg", "image.svg");
            }
            catch (Exception ex)
            {
                return View("Index");
            }
        }
    }
}