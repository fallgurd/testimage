﻿$(function () {
    $(".ajax_text_input").keyup(function () {
        $("#testform").submit();
    });
    $(".ajax_input").change(function () {
        $("#testform").submit();
    });
});

var showHelp = true;
function helpPressed(btn) {
    showHelp = !showHelp;
    document.getElementById("help").hidden = showHelp?"":"hidden";
}